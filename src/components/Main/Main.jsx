import React from 'react';
import styles from './Main.module.scss';

import { MainLeft } from '../MainLeft/MainLeft';
import { MainRight } from '../MainRight/MainRight';

export const Main = ({
  isDataLoading,
  fetchError,
  products,
  favorites,
  handleFavoritesClick,
  productsInBasket,
  handleBasketClick,
  setModal,
  setModalData,
}) => {
  return (
    <div className={styles.Main__row}>
      <MainLeft />
      <MainRight
        isDataLoading={isDataLoading}
        fetchError={fetchError}
        products={products}
        favorites={favorites}
        handleFavoritesClick={handleFavoritesClick}
        productsInBasket={productsInBasket}
        handleBasketClick={handleBasketClick}
        setModal={setModal}
        setModalData={setModalData}
      />
    </div>
  );
};

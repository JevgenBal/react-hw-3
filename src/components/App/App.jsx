import React, { useState, useEffect } from 'react';
import { useFeching } from '../../hooks/useFetching';
import { Service } from '../../API/Services';
import { Layout } from '../../Layout/Layout';

const favoriteIdsFromStorage = localStorage.getItem('favorites');
const basketIdsFromStorage = localStorage.getItem('basket');

export const App = () => {
  const [products, setProducts] = useState([]);

  const [favorites, setFavorites] = useState([]);

  const handleFavoritesClick = (id) => {
    if (favorites.includes(id)) {
      setFavorites((state) => state.filter((currentId) => currentId !== id));
    } else {
      setFavorites((state) => [...state, id]);
    }
  };

  useEffect(() => {
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }, [favorites]);

  const [productsInBasket, setProductsInBasket] = useState([]);

  const handleBasketClick = (id) => {
    if (productsInBasket.includes(id)) {
      setProductsInBasket((state) =>
        state.filter((currentId) => currentId !== id)
      );
    } else {
      setProductsInBasket((state) => [...state, id]);
    }
  };

  useEffect(() => {
    localStorage.setItem('basket', JSON.stringify(productsInBasket));
  }, [productsInBasket]);

  const [fetchData, isDataLoading, fetchError] = useFeching(async () => {
    const data = await Service.getAll();
    setProducts(data.appleproduct);
  });

  useEffect(() => {
    fetchData();

    if (favoriteIdsFromStorage) {
      setFavorites(JSON.parse(favoriteIdsFromStorage));
    }

    if (basketIdsFromStorage) {
      setProductsInBasket(JSON.parse(basketIdsFromStorage));
    }
  }, []);

  return (
    <Layout
      isDataLoading={isDataLoading}
      fetchError={fetchError}
      products={products}
      favorites={favorites}
      handleFavoritesClick={handleFavoritesClick}
      productsInBasket={productsInBasket}
      handleBasketClick={handleBasketClick}
    />
  );
};

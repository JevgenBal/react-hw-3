import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './Layout.module.scss';

import { Header } from '../components/Header/Header';
import { Main } from '../components/Main/Main';
import { Footer } from '../components/Footer/Footer';
import { MyModal } from '../UI/MyModal/MyModal';

export const Layout = ({
  isDataLoading,
  fetchError,
  products,
  favorites,
  handleFavoritesClick,
  productsInBasket,
  handleBasketClick,
}) => {
  const [modal, setModal] = useState(false);

  const [modalData, setModalData] = useState({
    header: '',
    closeButton: true,
    name: '',
    price: '',
    action: [
      {
        text: '',
        backgroundColor: '',
        onClick: null,
      },
    ],
  });

  return (
    <>
      <header className={styles.Header}>
        <div className={styles.container}>
          <Header favorites={favorites} productsInBasket={productsInBasket} />
        </div>
      </header>
      <main className={styles.Main}>
        <div className={styles.container}>
          <Main
            isDataLoading={isDataLoading}
            fetchError={fetchError}
            products={products}
            favorites={favorites}
            handleFavoritesClick={handleFavoritesClick}
            productsInBasket={productsInBasket}
            handleBasketClick={handleBasketClick}
            setModal={setModal}
            setModalData={setModalData}
          />
        </div>
      </main>
      <footer className={styles.Footer}>
        <div className={styles.container}>
          <Footer />
        </div>
      </footer>
      <MyModal visible={modal} setVisible={setModal} modalData={modalData} />
    </>
  );
};

Layout.propTypes = {
  product: PropTypes.shape({}).isRequired,
  favorites: PropTypes.array.isRequired,
  handleFavoritesClick: PropTypes.func.isRequired,
  productsInBasket: PropTypes.array.isRequired,
  handleBasketClick: PropTypes.func.isRequired,
};

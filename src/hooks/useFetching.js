import { useState } from 'react';
import PropTypes from 'prop-types';

export const useFeching = (callback) => {
  const [isLoading, setIsLoading] = useState(false);

  const [error, setError] = useState('');

  const fetching = async () => {
    try {
      setIsLoading(true);
      await callback();
    } catch (e) {
      setError(e.message);
    } finally {
      setIsLoading(false);
    }
  };

  return [fetching, isLoading, error];
};

useFeching.propTypes = {
  callback: PropTypes.func.isRequired,
};
